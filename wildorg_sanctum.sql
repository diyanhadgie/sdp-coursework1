-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 21, 2023 at 12:36 PM
-- Server version: 5.6.51
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wildorg_sanctum`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `isntitutionId` int(11) NOT NULL,
  `divisionId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `location` varchar(100) NOT NULL,
  `status` enum('1','2','3','4') NOT NULL DEFAULT '1' COMMENT '1 = Pending, 2 = Processing, 3 = Complete, 4 = Refused',
  `fileName` varchar(100) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `complaint_process`
--

CREATE TABLE `complaint_process` (
  `id` int(11) NOT NULL,
  `complaintId` int(11) NOT NULL,
  `processDescription` varchar(300) DEFAULT NULL,
  `processDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `officerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `institutionId` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `institutions`
--

CREATE TABLE `institutions` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `institutions`
--

INSERT INTO `institutions` (`id`, `name`, `description`) VALUES
(1, 'Wildlife', 'Wildlife Conservation'),
(2, 'Forest', 'Forest Conservation');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `officer`
--

CREATE TABLE `officer` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `institutionId` int(11) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officer`
--

INSERT INTO `officer` (`id`, `userId`, `institutionId`, `role`) VALUES
(1, 2, 1, 'Reporter');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `address` varchar(300) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `firstName`, `lastName`, `address`, `city`, `province`, `email`, `mobile`, `created_date`, `updated_date`) VALUES
(3, 'Public', 'User', '12 Jaffna', NULL, NULL, 'public@wild.org.lk', '0775123321', '2023-12-20 21:02:47', '2023-12-21 00:10:49'),
(2, 'Officer', 'User', '12 Anuradapura', NULL, NULL, 'officer@wild.org.lk', '0775123321', '2023-12-20 21:01:24', '2023-12-21 00:11:50'),
(1, 'Admin', 'User', '12 Colombo', NULL, 'Western', 'admin@wild.org.lk', '0775123321', '2023-12-20 21:00:23', '2023-12-21 00:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'MyApp', 'e085942c3ecdd398f9e46f5884e71609db9d659b26cf3361aa2f195ec2bc539a', '[\"*\"]', '2023-12-20 07:00:00', NULL, '2023-12-20 06:15:32', '2023-12-20 07:00:00'),
(2, 'App\\Models\\User', 1, 'MyApp', 'dc6777b95f74b32aea4d1074f958545313d5a67ae76e577bf6c3cc5297165212', '[\"*\"]', NULL, NULL, '2023-12-20 06:16:45', '2023-12-20 06:16:45'),
(3, 'App\\Models\\User', 1, 'MyApp', '9594eaae5f2a11470e283bcb575194e6a04a33069b291b4565b51e9885dac77a', '[\"*\"]', NULL, NULL, '2023-12-20 06:17:11', '2023-12-20 06:17:11'),
(4, 'App\\Models\\User', 1, 'MyApp', '29606fc82e51dbac9ed01f9f7c72491b4b163b442bad716e63c37c62c78dcc1a', '[\"*\"]', '2023-12-20 07:07:11', NULL, '2023-12-20 07:00:54', '2023-12-20 07:07:11'),
(5, 'App\\Models\\User', 3, 'MyApp', '1fb309474166eaa56c1f01662588a1aea280b3ad8a492ffc414d9b1f03c06ab7', '[\"*\"]', NULL, NULL, '2023-12-20 07:41:06', '2023-12-20 07:41:06'),
(6, 'App\\Models\\User', 3, 'MyApp', '5efecdc093903d296a85523bb0536c16fed51d20938b45049f0581068d3859a1', '[\"*\"]', NULL, NULL, '2023-12-20 07:43:23', '2023-12-20 07:43:23'),
(7, 'App\\Models\\User', 4, 'MyApp', '74c0c69f626f9baec24c2a854c8b85f83572c85378dc17503ccc94af3c3e8fef', '[\"*\"]', NULL, NULL, '2023-12-20 08:07:53', '2023-12-20 08:07:53'),
(8, 'App\\Models\\User', 5, 'MyApp', 'f75cebf6eaaf392bc529f7ca1d8478c1de8e479550344f2b716e282e0f4fefbd', '[\"*\"]', NULL, NULL, '2023-12-20 08:14:06', '2023-12-20 08:14:06'),
(9, 'App\\Models\\User', 6, 'MyApp', '9cb88d92baef76e7925ca1bc3b35d59f6d7dae4c123265ac4fd33ac40d965b6f', '[\"*\"]', NULL, NULL, '2023-12-20 08:15:46', '2023-12-20 08:15:46'),
(10, 'App\\Models\\User', 7, 'MyApp', 'edf9549a7f963191c852187c9efae0d156e7b9669a89190ad62db51bb8394714', '[\"*\"]', NULL, NULL, '2023-12-20 08:16:30', '2023-12-20 08:16:30'),
(13, 'App\\Models\\User', 16, 'MyApp', 'f1a4ec607dd88e76c5f93ae6e71b27c332bc27323e7130523e8a0cc55c462847', '[\"*\"]', '2023-12-20 13:03:00', NULL, '2023-12-20 12:35:15', '2023-12-20 13:03:00'),
(14, 'App\\Models\\User', 1, 'MyApp', '85cfe782e60f98997f1edda63589641c9996e4755c7ffdad9c1512a6879fa0b8', '[\"*\"]', NULL, NULL, '2023-12-20 15:48:08', '2023-12-20 15:48:08'),
(15, 'App\\Models\\User', 1, 'MyApp', '12f07aaead68870db41af02aa2c5dede9a9f2d4005c3042ba19ab08035f86a4e', '[\"*\"]', NULL, NULL, '2023-12-20 16:00:05', '2023-12-20 16:00:05'),
(16, 'App\\Models\\User', 1, 'MyApp', '6b80969695061babf156d2be4a6e0a602a91f40036eab2514b7fff406fed9805', '[\"*\"]', NULL, NULL, '2023-12-20 16:17:54', '2023-12-20 16:17:54'),
(17, 'App\\Models\\User', 1, 'MyApp', '412efb79dd4d8f30a07ebb98b37df25a0a1a20fcd4b3920b84f6889cf2fbbde4', '[\"*\"]', NULL, NULL, '2023-12-20 16:18:08', '2023-12-20 16:18:08'),
(18, 'App\\Models\\User', 1, 'MyApp', '451bbc4ff219c3a1ad24769211a0a5e2b0fb8e343fccdbfa9a5a2ccfd0da0f05', '[\"*\"]', NULL, NULL, '2023-12-20 16:20:56', '2023-12-20 16:20:56'),
(19, 'App\\Models\\User', 1, 'MyApp', '186beff93802739793e8714984660960b1f397e2a973658a26d720c514d214a2', '[\"*\"]', NULL, NULL, '2023-12-20 16:31:29', '2023-12-20 16:31:29'),
(20, 'App\\Models\\User', 1, 'MyApp', '85ff66dc3163f972762034e7342ba3175e030d1272bed9f8a1c23d0e02bf8856', '[\"*\"]', NULL, NULL, '2023-12-20 16:33:04', '2023-12-20 16:33:04'),
(21, 'App\\Models\\User', 1, 'MyApp', '5e6924160ee672c2ec127fa90ea634221358f5d1238d9c87e6f0686bd5a34818', '[\"*\"]', NULL, NULL, '2023-12-20 20:00:16', '2023-12-20 20:00:16'),
(22, 'App\\Models\\User', 1, 'MyApp', '41f22ab75f936b54ba648b1004c588f885556faadb5a9f7cc06459a9897e4823', '[\"*\"]', NULL, NULL, '2023-12-20 20:00:46', '2023-12-20 20:00:46'),
(23, 'App\\Models\\User', 1, 'MyApp', '003be547915827709dfe4ea6c2fefb4c290105542a9d2105884a066a2f29395f', '[\"*\"]', NULL, NULL, '2023-12-20 20:04:42', '2023-12-20 20:04:42'),
(24, 'App\\Models\\User', 3, 'MyApp', '648734750e11ecc965c401829a0c8b042d1f0013f9505e3aea53b92cc7215aac', '[\"*\"]', NULL, NULL, '2023-12-20 21:51:11', '2023-12-20 21:51:11'),
(25, 'App\\Models\\User', 3, 'MyApp', 'b9ff1ed97749164d23af6af33072b614adbf2535b99f271c55f4f84ac5b77d7f', '[\"*\"]', NULL, NULL, '2023-12-20 21:56:05', '2023-12-20 21:56:05'),
(26, 'App\\Models\\User', 1, 'MyApp', 'cdb5fae22753ca51c4219d7afab59b9ca40c2c378c9cb8d8f669132a48fc371f', '[\"*\"]', NULL, NULL, '2023-12-20 22:00:53', '2023-12-20 22:00:53'),
(27, 'App\\Models\\User', 1, 'MyApp', 'a1a28f1e91f709aff06c1a9752a09a5fb88fe2898c44bd24186a35358f5629f2', '[\"*\"]', NULL, NULL, '2023-12-20 22:01:14', '2023-12-20 22:01:14'),
(28, 'App\\Models\\User', 1, 'MyApp', '9d5d0b0a221b74fd33277dda8d2658b24564c8ba7c7ff818260eea43d291add4', '[\"*\"]', NULL, NULL, '2023-12-20 22:31:01', '2023-12-20 22:31:01'),
(29, 'App\\Models\\User', 1, 'MyApp', 'adbfdbaa6e28b2ee4d3ce70174cb055823edbb6ae58c6dcc86bafaa39895d466', '[\"*\"]', NULL, NULL, '2023-12-20 22:56:07', '2023-12-20 22:56:07'),
(30, 'App\\Models\\User', 1, 'MyApp', 'a6574bc714c55e20cfed46e34d079fc10e9827659791c24ab02aea9ee09be64b', '[\"*\"]', NULL, NULL, '2023-12-20 23:14:26', '2023-12-20 23:14:26'),
(31, 'App\\Models\\User', 1, 'MyApp', '158e1082208d9a9f3e8840b18a335d5523fd1cc0be138f93facfc364cbae1fc9', '[\"*\"]', NULL, NULL, '2023-12-20 23:44:36', '2023-12-20 23:44:36'),
(32, 'App\\Models\\User', 1, 'MyApp', 'c8921e7ed24b301c3453a1e38a1b4e5d63099995ab3c7926aca551f1bcc09e9d', '[\"*\"]', NULL, NULL, '2023-12-20 23:47:22', '2023-12-20 23:47:22'),
(33, 'App\\Models\\User', 1, 'MyApp', 'b3c6e11abf652aa406c7bde9cd73c98da3f4d55046d79366036fce4d72a42897', '[\"*\"]', NULL, NULL, '2023-12-21 00:00:36', '2023-12-21 00:00:36'),
(34, 'App\\Models\\User', 1, 'MyApp', 'bf3f63dfc5da58077a80e281e2e4a3b106e01732e5f2f83833980aaa989ce43e', '[\"*\"]', NULL, NULL, '2023-12-21 00:27:26', '2023-12-21 00:27:26'),
(35, 'App\\Models\\User', 1, 'MyApp', 'f3c63f0d0f205ccacc235a66678e0d5fb220be88fd54f871fe89a60a63899482', '[\"*\"]', NULL, NULL, '2023-12-21 00:31:52', '2023-12-21 00:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3',
  `personId` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `type`, `personId`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@wild.org.lk', NULL, '$2y$10$AguQDD31ywrok2XCgMxz0.cb6gCL6OCPCdmAkUWXSe4aGWSoVYqn.', NULL, '1', 1, '2023-12-20 06:15:32', '2023-12-20 06:15:32'),
(2, 'Officer', 'officer@wild.org.lk', NULL, '$2y$10$QbyxNlkLoHhwfolMFjjKeuFxv68W29eZfdG/ujEZZLsx0yApXUyqG', NULL, '2', 2, '2023-12-20 07:41:06', '2023-12-20 07:41:06'),
(3, 'Public', 'public@wild.org.lk', NULL, '$2y$10$ZYKQNFga7EhkcDsGpZ.nruh9uqL580LHX17ddCvpRpkEDfAijFly6', NULL, '3', 3, '2023-12-20 08:07:53', '2023-12-20 08:07:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_process`
--
ALTER TABLE `complaint_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutions`
--
ALTER TABLE `institutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officer`
--
ALTER TABLE `officer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`(191),`tokenable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `complaint_process`
--
ALTER TABLE `complaint_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institutions`
--
ALTER TABLE `institutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `officer`
--
ALTER TABLE `officer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
