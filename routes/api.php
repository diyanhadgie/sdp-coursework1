<?php

use App\Http\Controllers\API\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RegisterController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** ---------Register and Login ----------- */
Route::controller(RegisterController::class)->group(function()
{
    Route::post('register', 'register');
    Route::post('login', 'login');
    Route::post('users', 'login')->name('index');
    // Route::post('/createOfficer','createOfficer')->name('createOfficer');
});
// Route::apiResource('/createOfficer', RegisterController::class)->middleware('auth:sanctum');
/** -----------Users --------------------- */
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/users',[RegisterController::class,'index'])->name('index');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::post('/createOfficer',[RegisterController::class,'createOfficer'])->name('createOfficer');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::post('/setComplaint',[RegisterController::class,'setComplaint'])->name('setComplaint');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/getComplaint',[RegisterController::class,'getComplaint'])->name('getComplaint');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/getComplaint/{complaintId}',[RegisterController::class,'getComplaint'])->name('getComplaint');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/getUserData',[RegisterController::class,'getUserData'])->name('getUserData');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::post('/editUserData/{personId}',[RegisterController::class,'editUserData'])->name('editUserData');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::post('/assignJob/{complaintId}',[RegisterController::class,'assignJob'])->name('assignJob');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/getAllOfficers',[RegisterController::class,'getAllOfficers'])->name('getAllOfficers');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/getComplaintSummary',[RegisterController::class,'getComplaintSummary'])->name('getComplaintSummary');
});
Route::middleware('auth:sanctum')->group(function() {
    Route::get('/logout',[RegisterController::class,'logout'])->name('logout');
});


Route::middleware('auth:sanctum')->controller(RegisterController::class)->group(function() {
    Route::get('/users','index')->name('index');
});
