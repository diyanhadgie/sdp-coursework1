<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;


class Person extends Model
{
    use HasFactory;


    public static function getUserId($userId)
    {
        return DB::table('users')->where('personId', $userId)->value('id');
    }

    public static function getUserType($userId)
    {
        return DB::table('users')->where('personId', $userId)->value('type');
    }

    public static function getUserData($userId)
    {
        $userData = DB::table('person')
            ->select('person.*', 'user.type')
            ->leftJoin('users', 'person.id', '=', 'user.personId')
            ->where('user.personId', $userId)
            ->get();
        return $userData;
    }

    public static function updateUserData($userId, $userData)
    {
        return DB::table('person')
            ->where('id', $userId)
            ->update($userData);
    }

    public static function deactivateUser($userId)
    {
        return DB::table('person')
            ->where('id', $userId)
            ->update(['status' => 0]);
    }

    public static function getAllOfficers()
    {
        $users = DB::table('user')
            ->select('user.username', 'user.type', 'person.*', 'officer.role', 'officer.id')
            ->join('person', 'user.personId', '=', 'person.id')
            ->leftJoin('officer', 'user.id', '=', 'officer.userId')
            ->whereIn('user.type', [1, 2])
            ->get();

        return $users;
    }
}
