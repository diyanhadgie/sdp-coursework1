<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Person;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\HasApiTokens;


class RegisterController extends BaseController

{
    use HasApiTokens;
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    /** get all users */
    public function index()
    {
        $users = User::all();
        return $this->sendResponse($users, 'Displaying all users data');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'lastName' => 'required|string',
            'address' => 'required|string',
            'mobile' => 'required|string',
        ]);

        if ($validator->fails()) {
            // return $this->sendError('Validation Error.', $validator->errors());
            return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
        }

        $input = $request->all();
        $personData = Arr::except($input, ['password', 'c_password', 'name']);
        $userData = Arr::except($input, ['mobile', 'address', 'lastName']);
        // $response = Person::insertUser($personData);
        // Check user available or not
        $registeredUser = DB::table('users')->where('email', $personData['email'])->first();

        if ($registeredUser) {
            return $response = ['success' => false, 'message' => 'Email already exists'];
        } else {

            $personData['firstName'] = $input['name'];
            $personId = DB::table('person')->insertGetId($personData);
            // return response()->json(['success' => true, 'message' => $personId], 200);

            $userData['personId'] = $personId;
            $userData['password'] = bcrypt($input['password']);
            $userData['type'] = 3;
            $user = User::create($userData);
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;
            return response()->json(['success' => true, 'message' => 'User created successfully'], 200);
        }
        return response()->json(['success' => false, 'message' => 'User creation failed'], 401);
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;
            return $this->sendResponse($success, 'User login successfully.');
        } else {
            return $this->sendError('Unauthorised.', ['error' => 'Unauthorised']);
        }
    }

    public function createOfficer(Request $request)
    {
        if ($request->user()) {

            // return $response = ['success' => false, 'message' => $request->header()];
            $validator = Validator::make($request->all(), [
                'firstName' => 'required|string',
                'lastName' => 'required|string',
                'address' => 'required|string',
                'city' => 'nullable|string',
                'province' => 'nullable|string',
                'type' => 'required|numeric|in:1,2',
                'email' => 'required|email',
                'mobile' => 'required|string',
                'role' => 'nullable|string',
                'conservation' => 'nullable|numeric',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
            }

            $validatedData = $validator->validated();
            // $response = Person::createOfficer($validatedData);

            $personData = Arr::except($validatedData, ['password', 'c_password', 'role', 'username', 'type', 'conservation']);
            $registeredUser = DB::table('person')->where('email', $validatedData['email'])->first();

            if ($registeredUser) {
                return $response = ['success' => false, 'message' => 'Email already exists'];
                // error_log('Email already exists for ' . $userData['email']);
            } else {

                $personId = DB::table('person')->insertGetId($personData);

                $userData['personId'] = $personId;
                $userData['password'] = bcrypt($validatedData['password']);
                $userData['email'] = $validatedData['email'];
                $userData['type'] = 2;
                $userData['name'] = $validatedData['firstName'];
                // $userId = DB::table('user')->insertGetId($userData);
                $user = User::create($userData);
                $officerData['userId'] = $user->id;
                $officerData['role'] = $validatedData['role'];

                $officerData['userId'] = $user->id;
                $officerData['institutionId'] = $validatedData['conservation'];
                $insertOfficer = DB::table('officer')->insert($officerData);

                return response()->json(['success' => false, 'message' => 'Officer created successfully'], 201);
            }
            return response()->json(['success' => false, 'message' => 'User creation failed'], 500);
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function setComplaint(Request $request)
    {
        if ($request->user()) {
            // return response()->json(['success' => false, 'errors' => $request->user()->id], 422);
            $userId = $request->user()->id;
            $validator = Validator::make($request->all(), [
                'title' => 'required|string',
                'description' => 'required|string',
                'location' => 'required|string',
                'imageFiles' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:2048'],
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
            }

            $validatedData = $validator->validated();
            $validatedData['userId'] = $userId;

            // $response = Complaint::saveComplaint($validatedData);

            if ($validatedData) {
                try {
                    $complaintId = DB::table('complaints')->insertGetId([
                        'title' => $validatedData['title'],
                        'description' => $validatedData['description'],
                        'location' => $validatedData['location'],
                        'userId' => $validatedData['userId']
                    ]);

                    $response = ['success' => true, 'message' => (object)['id' => $complaintId]];
                } catch (\Exception $e) {
                    $response['message'] = $e->getMessage();
                }
            }

            if (isset($response['success']) && $response['success'] === true) {
                if ($request->hasFile('imageFiles')) {

                    $file = $request->file('imageFiles');
                    $path = $file->storeAs('', $response['message']->id . '.' . $file->getClientOriginalExtension());

                    $fileName = $response['message']->id . '.' . $file->getClientOriginalExtension();
                    if (!$path) {
                        return response()->json(['success' => false, 'message' => 'Failed to store file'], 500);
                    }
                    // $complaint->update(['fileName' => $fileName]);
                    // Complaint::updateFileName($response['message']->id, $fileName);
                    DB::table('complaints')
                        ->where('id', $response['message']->id)
                        ->update(['fileName' => $fileName]);
                }
                $validatedData['fileLocation'] = '/storage/app/' . $fileName;
                return response()->json(['success' => true, 'message' => 'Complaint saved successfully', 'userId' => $userId, 'validatedData' => $validatedData], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'Failed to save complaint'], 500);
            }
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function getComplaint(Request $request)
    {
        if ($request->user()) {
            $complaintId = isset($request->complaintId) ? $request->complaintId : null;
            $userId = $request->user()->id;
            if (!$userId) {
                return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
            }
            $complaint = DB::table('complaints')
                ->select('*')
                ->where('userId', $userId);

            if ($complaintId !== null) {
                $complaint->where('id', $complaintId);
            }

            $complaints = $complaint->get();

            return response()->json(['success' => true, 'user' => $userId, 'data' => ['complaints' => $complaints]], 200);
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function getUserData(Request $request)
    {
        if ($request->user()) {
            $userId = $request->user()->id;
            $userData = DB::table('person')
                ->select('person.*', 'users.type')
                ->leftJoin('users', 'person.id', '=', 'users.personId')
                ->where('users.id', $userId);
            $response = $userData->get();
            return response()->json(['success' => true, 'user' => $userId, 'data' => ['userData' => $response]], 200);
        }

        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function assignJob(Request $request)
    {
        if ($request->user() && $request->user()->type == '1') {
            $validator = Validator::make($request->all(), [
                'officerId' => 'required|string',
                'processDescription' => 'nullable|string',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
            }
            $complaintId = $request->complaintId;
            $officerId = $request->officerId;
            $description = $request->processDescription;
            $insertComplaint =  DB::table('complaint_process')->insert([
                'complaintId' => $complaintId,
                'processDescription' => $description,
                'officerId' => $officerId,
            ]);
            $updateComplaintStatus = DB::table('complaints')
                ->where('id', $complaintId)
                ->update(['status' => 2]);
            if ($insertComplaint && $updateComplaintStatus) {
                return response()->json(['success' => true, 'message' => 'Complaint assigned successfully'], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'Failed to assign complaint'], 500);
            }
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function getAllOfficers(Request $request)
    {
        if ($request->user() && $request->user()->type == '1') {
            $users = DB::table('users')
                ->select('users.email', 'users.type', 'person.*', 'officer.role', 'officer.id')
                ->join('person', 'users.personId', '=', 'person.id')
                ->leftJoin('officer', 'users.id', '=', 'officer.userId')
                ->whereIn('users.type', [1, 2])
                ->get();
            return response()->json(['success' => false, 'data' => $users], 201);
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function getComplaintSummary(Request $request)
    {
        if ($request->user()) {
            $summary = DB::table('complaints')
                ->select('status', DB::raw('COUNT(*) as count'))
                ->groupBy('status')
                ->get();
            $summary['status 1'] = 'Pending';
            $summary['status 2'] = 'Processing';
            $summary['status 3'] = 'Finished';
            $summary['status 4'] = 'Refused';
            return response()->json(['success' => true, 'summary' => $summary], 200);
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }

    public function editUserData(Request $request)
    {
        if ($request->user() && $request->user()->personId == $request->personId) {
            $user = $request->user();

            $validator = Validator::make($request->all(), [
                'firstName' => 'required|string',
                'lastName' => 'required|string',
                'address' => 'required|string',
                'mobile' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()], 422);
            }
            $editUserId = $request->personId;

            $validatedData = $validator->validated();
            $response = DB::table('person')
                ->where('id', $editUserId)
                ->update($validatedData);
            
                // return response()->json(['success' => true, 'message' => $user->personId]);
            if ($response) {
                return response()->json(['success' => true, 'message' => 'User data updated successfully'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Failed to update user data'], 500);
            }
        }
        return response()->json(['success' => false, 'message' => 'User not authenticated'], 401);
    }


    public function logout(Request $request)
    {
        $user = $request->user();
        $logout = $user->tokens()->delete();
        return response()->json(['success' => true, 'message' => 'User signed out'], 200);
    }
}
